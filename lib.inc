section .data
new_line_char: db 10

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	xor rdi, rdi
	syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.count_loop:
		cmp byte [rdi + rax], 0
		jz .break_loop
		inc rax
		jmp .count_loop
	.break_loop:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
	call string_length
    pop rdi
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	mov rsi, rsp
	syscall
	pop rdi	
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, new_line_char
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
	xor rcx, rcx
	mov r8, 10			;divider
	mov rax, rdi
	.divide_loop:		;divide number and push in stack
		xor rdx, rdx
		div r8
		push rdx
		inc rcx
		test rax, rax
		jz .print_loop
		jmp .divide_loop
	.print_loop:		;pop from stack and print digit as char
		pop rax
		mov rdi, rax
		add rdi, '0'
		push rcx
		call print_char
		pop rcx
		dec rcx
		test rcx, rcx
		jnz .print_loop
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
	cmp rdi, 0
	js .print_negative			;check negative number
	.print_positive:			;print positive number
        jmp print_uint
	.print_negative:			;print '-' then print -number
		neg rdi
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		jmp .print_positive	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
	mov r10, rdi			;r10, r11 - strings. r8, r9 - lengths, in main loop - chars of strings 
	mov r11, rsi
    push r11
	call string_length
    pop r11
	mov r8, rax
	mov rdi, r11
    push r11
	call string_length
    pop r11
	mov r9, rax
	cmp r8, r9				;compare strings length
	jnz .ret_error
	xor rcx, rcx			;rcx - counter, count for rax(length)
	.main_loop:
		cmp rcx, rax
		jz .ret_ok
		mov r8b, byte [r10 + rcx]
		mov r9b, byte [r11 + rcx]
		cmp r8b, r9b
		jnz .ret_error
		inc rcx
		jmp .main_loop
	.ret_ok:
		mov rax, 1
		ret
	.ret_error:
		mov rax, 0
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push rdx
	mov rax, 0
	mov rdi, 0
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	pop rdx
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

check_space_char:
	cmp rdi, ' '
	jz .ret_space_code
	cmp rdi, '	' 
	jz .ret_space_code
	cmp rdi, '\n'
	jz .ret_space_code
    .ret_not_space_code:
		mov rax, 0
		ret
	.ret_space_code:
		mov rax, 1
		ret

read_word:
	xor rdx, rdx
	xor rax, rax
	xor rcx, rcx
	xor r8, r8		
	.read_spaces_loop:
		push rdi
		push rcx
		push rsi
		call read_char
		pop rsi
		pop rcx
		pop rdi
		mov r8, rax
		cmp r8, 0x0
		jz .ret_error_eof
		push rdi
		mov rdi, r8
		call check_space_char
		pop rdi
		cmp rax, 1
		jz .read_spaces_loop
		inc rcx
		cmp rcx, rsi
		jae .ret_error_out_of_bounds
		mov [rdi + rcx - 1], r8
	.read_word_loop:
		push rdi
		push rcx
		push rsi
		call read_char
		pop rsi
		pop rcx
		pop rdi
		mov r8, rax
		
		push rdi
		mov rdi, r8
		call check_space_char
		pop rdi
		cmp rax, 1
		jz .ret_ok_word
		cmp r8, 0
		jz .ret_ok_word

		inc rcx
		cmp rcx, rsi
		jae .ret_error_out_of_bounds
		mov [rdi + rcx - 1], r8
		jmp .read_word_loop
	.ret_ok_word:
		mov byte [rdi + rcx], 0x0
		mov rax, rdi
		mov rdx, rcx
		ret
	.ret_error_eof:
		mov rax, 0
		ret
	.ret_error_out_of_bounds:
		mov rax, 0
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

is_digit:
	cmp rdi, '0'
	jl .ret_not_digit
	cmp rdi, '9'
	ja .ret_not_digit
	.ret_digit:
		mov rax, 1
		ret
	.ret_not_digit:
		mov rax, 0
		ret

parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor r9, r9 
    mov r10, 10         ;multiplier 
    .main_loop:
        mov r9b, byte [rdi + rcx]
        push rdi
        push rax
        mov rdi, r9
        call is_digit
        test rax, rax
        jz .break
        sub r9, '0'
        pop rax
        pop rdi
        mul r10
        add rax, r9
        inc rcx
        jmp .main_loop
    .break:
        pop rax
        pop rdi
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
    cmp byte [rdi], '-'
    jnz .positive
    .negative:
        push rdi
        inc rdi
        call parse_uint
        pop rdi
        neg rax
        inc rdx
        ret
    .positive:
        jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .check_length:
        push rdi
        push rsi
        push rdx
        call string_length
        pop rdx
        pop rsi
        pop rdi
        inc rax
        cmp rax, rdx
        ja .ret_error_length
    .main_loop:
        mov r8b, [rdi + rcx]
        mov byte[rsi + rcx], r8b
        cmp r8b, 0
        jz .break
        inc rcx
        jmp .main_loop
    .break:
        inc rcx
        mov rax, rcx
        ret
    .ret_error_length:
        xor rax, rax
        ret

